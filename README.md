# NotificationIonicApp

#### Instalation

##### Node & Bower

```
https://nodejs.org/en/download/
$ npm install -g bower
```

##### Ionic & Cordova

```
$ npm install -g cordova ionic
$ npm i -D -E @ionic/v1-toolkit
```


##### Karma (unit-tests)

```
$ npm install karma karma-jasmine karma-phantomjs-launcher --save-dev
$ npm install -g karma-cli
$ npm install
$ bower install angular-mocks --save-dev
  Unable to find a suitable version for angular, please choose one by typing one of the numbers below
  1) angular#1.5.3 which resolved to 1.5.3 and is required by angular-animate#1.5.3, angular-mocks#1.5.3, angular-sanitize#1.5.3, ionic#1.3.4
  2) ...
  $ 1!
  Unable to find a suitable version for angular-mocks, please choose one by typing one of the numbers below:
  1) angular-mocks#1.5.3 which resolved to 1.5.3 and is required by NotificationIonicApp
  2) ...
  $ 1!
$ bower install --force
```


##### Protractor (e2e-tests)

```
$ npm install -g protractor
$ webdriver-manager update
  other options:
    to solve a problem and use only Chrome:
    $ webdriver-manager update --gecko=false
```


#### Run

##### App

```
$ ionic serve --lab
  other options:
    $ ionic serve
    $ ionic serve --nobrowser
    if ionic not installed before:
      $ Install @ionic/v1-toolkit? yes
```


##### Unit Test

```
With the app running, in another terminal
$ cd tests
$ karma start unit-tests.conf.js
  if the file is not created:
    $ karma init unit-tests.conf.js
```


##### E2E Test

```
With the app running, in another terminal
$ cd tests
$ protractor e2e-tests.conf.js
```


#### Data Server

```
https://console.firebase.google.com/u/0/project/notification-ionic-app/database/notification-ionic-app/data
```

