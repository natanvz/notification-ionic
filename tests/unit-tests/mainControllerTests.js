describe('MainController', function() {

	var controller, 
		deferredLogin,
		scopeMock,
		ionicHistoryMock,
		cordovaCameraMock,
		ionicPopupMock,
		windowMock,
		NotificationServiceMock,
		PictureServiceMock
		;

	// load the module for our app
	beforeEach(module('app'));
	
	beforeEach(inject(function($controller, $q) {
		deferredLogin = $q.defer();

		windowMock = window;
		scopeMock = jasmine.createSpyObj('$scopeMock spy', ['']);
		ionicHistoryMock = jasmine.createSpyObj('$ionicHistoryMock spy', ['currentView']);
		cordovaCameraMock = jasmine.createSpyObj('$cordovaCameraMock spy', ['']);
		ionicPopupMock = jasmine.createSpyObj('$ionicPopup spy', ['']);
		
		NotificationServiceMock = jasmine.createSpyObj('NotificationServiceMock spy', ['']);
		PictureServiceMock = jasmine.createSpyObj('PictureServiceMock spy', ['']);

		// instantiate MainController
		controller = $controller('MainController', { 
			'$scope': scopeMock,
			'$ionicHistory': ionicHistoryMock,
			'$cordovaCamera': cordovaCameraMock,
			'$ionicPopup': ionicPopupMock, 
			'$windowMock': windowMock, 
			'NotificationService': NotificationServiceMock,
			'PictureService': PictureServiceMock
		});

	}));

	describe('#navigate', function() {

		var url = '#teste';

		// call navigate on the controller for every test
		beforeEach(inject(function(_$rootScope_) {
			$rootScope = _$rootScope_;
			scopeMock.navigate(url);
		}));

		describe('when the navigate is executed,', function() {
			it('should navigate to the given url', function() {
				expect(windowMock.location.href.includes(url)).toBeTruthy();	
			});
		});

	});

	describe('#back', function() {

		var url = '#teste';
		var baseUrl = '';

		// call back on the controller for every test
		beforeEach(inject(function(_$rootScope_) {
			$rootScope = _$rootScope_;
			baseUrl = windowMock.location.href;
			scopeMock.navigate(url);
			scopeMock.back();
		}));

		describe('when the back is executed,', function() {
			it('should go back to the previous url', function() {
				expect(windowMock.location.href.includes(baseUrl)).toBeTruthy();	
			});
		});

	});
	
});