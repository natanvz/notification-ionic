describe('NotificationService', function() {
	var notificationService;

	beforeEach(module('app'));

	beforeEach(inject(function($injector) {
		notificationService = createService = function() {
			return $injector.get('NotificationService');        
		}();
	}));

	describe('#checkUnreadNotifications', function() {
		it('if there is no notifications, it should set unreadNotifications as false', function() {
			notificationService.setAll([]);
			notificationService.checkUnreadNotifications();
			expect(notificationService.hasUnreadNotifications()).toBeFalsy();
		});
		it('if there is only read notifications, it should set unreadNotifications as false', function() {
			notificationService.setAll([{read:true},{read:true}]);
			notificationService.checkUnreadNotifications();
			expect(notificationService.hasUnreadNotifications()).toBeFalsy();
		});
		it('if there is at least one unread notification, it should set unreadNotifications as true', function() {
			notificationService.setAll([{read:true},{read:true},{read:false}]);
			notificationService.checkUnreadNotifications();
			expect(notificationService.hasUnreadNotifications()).toBeTruthy();
		});
	});

});