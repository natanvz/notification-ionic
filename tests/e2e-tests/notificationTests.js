// Slow down actions - begin
var origFn = browser.driver.controlFlow().execute;

browser.driver.controlFlow().execute = function() {
  var args = arguments;

  // queue 100ms wait
  origFn.call(browser.driver.controlFlow(), function() {
    return protractor.promise.delayed(100);
  });

  return origFn.apply(browser.driver.controlFlow(), args);
};
// Slow down actions - end

describe('Read (and unread) notifications', function(){
	
	beforeEach(function() {
	});
	
	// In fact for development every time a notification is visualized its status is changed 
	// to the opposite (read to unread and unread to read)
	describe('#read a message', function() {
		
		var notificationStatusBeforeTest;

		it('should navigate to notifications tab', function() {

			browser.get('');

			var notificationsTab = element(by.id('Notifications'));

			browser.actions().mouseUp(notificationsTab).perform();	

			// Workaround for click on tab button	
			browser.executeScript("window.location = document.getElementById('Notifications').getAttribute('href')");
		});
		it('should click on a notification (the first)', function() {
			var notificationItem = element.all(by.css('ion-item')).get(0);
			var unreadNotificationItem = notificationItem.element(by.css('.icon-notification-unread'));
			notificationStatusBeforeTest = unreadNotificationItem.isPresent();
			browser.actions().mouseMove(notificationItem).perform();
			notificationItem.click();
		});
		it('should navigate back to the notifications tab', function() {
			var backButton = element(by.id('backButton'));
			browser.actions().mouseMove(backButton).perform();
			backButton.click();
		});
		it('should see the notification as read if it was unread before or unread if it was read before', function() {
			var notificationItem = element.all(by.css('ion-item')).get(0);
			var unreadNotificationItem = notificationItem.element(by.css('.icon-notification-unread'));
			var notificationStatusAfterTest = unreadNotificationItem.isPresent();
			expect(notificationStatusBeforeTest != notificationStatusAfterTest);
		});
	});
	
});