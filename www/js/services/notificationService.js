// angular.module('app.services')
angular.module('app')
.factory('NotificationService', function ($http, $interval) {

  var notifications = [];
  var unreadNotifications = false;

  getAll = function () {
    return notifications;
  }
  
  setAll = function (pNotifications) {
    notifications = pNotifications;
  }

  getNewNotifications = function () {
    $http({
      method: 'GET',
      url: 'https://notification-ionic-app.firebaseio.com/rest/notifications.json'
    }).success(function (data) {
      notifications = data;
      notifications.sort(function(a, b) {
        return new Date(b.date) - new Date(a.date);
      });
      checkUnreadNotifications();
    }).error(function (data) {
      console.error(data);
    })
  };

  checkNewNotifications = function () {
    getNewNotifications();
    $interval(function () {
      checkNewNotifications();
    }, 300000); // 5 minutes
  };

  checkNewNotifications();

  hasUnreadNotifications = function () {
    return unreadNotifications;
  };

  checkUnreadNotifications = function () {
    unreadNotifications = false;
    if (notifications) {
      for (var i = 0; i < notifications.length; i++) {
        if (!notifications[i].read) {
          unreadNotifications = true;
          return;
        }
      }
    }
  };

  get = function (notificationId) {
    for (var i = 0; i < notifications.length; i++) {
      if (notifications[i].id === parseInt(notificationId)) {
        return notifications[i];
      }
    }
    return null;
  };

  put = function (notification) {
    for (var i = 0; i < notifications.length; i++) {
      if (notifications[i].id === parseInt(notification.id)) {
        $http({
          method: 'PATCH',
          url: 'https://notification-ionic-app.firebaseio.com/rest/notifications/' + i + '.json',
          data: notification
        }).success(function (data) {
          notifications[i] = notification;
          checkUnreadNotifications();
        }).error(function (data) {
          console.error(data);
        })
        return;
      }
    }
  }

  remove = function (notification) {
    // TODO
  };

  return {
    getAll, setAll, hasUnreadNotifications, checkUnreadNotifications, get, put, remove, getNewNotifications
  };

});