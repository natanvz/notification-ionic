// angular.module('app.services')
angular.module('app')
.factory('PictureService', function ($http) {

  var picture = null;

  get = function (id) {
    $http({
      method: 'GET',
      url: 'https://notification-ionic-app.firebaseio.com/rest/pictures/' + id + '.json'
    }).success(function (data) {
      picture = data.data;
    }).error(function (data) {
      console.error(data);
    })
  };

  get(0);

  getMain = function () {
    return picture;
  };

  put = function (pictureData) {
    var id = 0;
    $http({
      method: 'PATCH',
      url: 'https://notification-ionic-app.firebaseio.com/rest/pictures/' + id + '.json',
      data: { id: 0, data: pictureData }
    }).success(function (data) {
      picture = data.data;
    }).error(function (data) {
      console.error(data);
    })
  };

  return {
    get, getMain, put
  }

});