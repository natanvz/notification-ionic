// NotificationIonicApp

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'NotificationIonicApp' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'app.services' is found in services.js
// 'app.controllers' is found in controllers.js
// angular.module('app', ['ionic', 'ngCordova', 'firebase', 'app.controllers', 'app.services'])
//angular.module('app', ['ionic', 'ngCordova', 'firebase'])
angular.module('app', ['ionic', 'ngCordova'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs).
      // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
      // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
      // useful especially with forms, though we would prefer giving the user a little more room
      // to interact with the app.
      if (window.cordova && window.Keyboard) {
        window.Keyboard.hideKeyboardAccessoryBar(true);
      }

      if (window.StatusBar) {
        // Set the statusbar to use the default style, tweak this to
        // remove the status bar on iOS or change it to use white instead of dark colors.
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    $ionicConfigProvider.tabs.position('bottom');

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'views/tabs.html'
      })
      .state('tab.home', {
        url: '/home',
        views: {
          'home-tab': {
            templateUrl: 'views/home-tab.html',
            controller: 'HomeController'
          }
        }
      })
      .state('tab.notifications', {
        url: '/notifications',
        views: {
          'notifications-tab': {
            templateUrl: 'views/notifications-tab.html',
            controller: 'NotificationController'
          }
        }
      })
      .state('tab.notification-detail', {
        url: '/notifications/:notificationId',
        views: {
          'notifications-tab': {
            templateUrl: 'views/notification-detail.html',
            controller: 'NotificationController'
          }
        }
      });
    $urlRouterProvider.otherwise('/tab/home');

  });

angular.module('app.controllers', []);
angular.module('app.services', []);