// (function() {
// angular.module('app.controllers')
angular.module('app')
.controller('NotificationController', function ($scope, $stateParams, NotificationService) {
//.controller('NotificationController', ['$scope', '$stateParams', 'NotificationService', NotificationController]);
//function NotificationController($scope, $stateParams, NotificationsService) {

  $scope.notification = NotificationService.get($stateParams.notificationId);

  $scope.getNotifications = function () {
    return NotificationService.getAll();
  }

  $scope.get = function (notificationId) {
    $scope.notification = NotificationService.get(notificationId);
  };

  $scope.read = function (notificationId) {
    $scope.notification = NotificationService.get(notificationId);
    $scope.notification.read = !$scope.notification.read;
    NotificationService.put($scope.notification);
    window.location = "#/tab/notifications/" + notificationId;
  };

  $scope.timeSince = function (dateStr) {
    date = new Date(dateStr);
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return date.toLocaleDateString();
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + " hours ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + " minutes ago";
    }
    return Math.floor(seconds) + " seconds ago";
  };

});
//}
//})();