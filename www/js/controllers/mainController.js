// (function() {

// angular.module('app.controllers')
angular.module('app')
.controller('MainController', function ($scope, $ionicHistory, $cordovaCamera, $ionicPopup, $window, NotificationService, PictureService) {
// .controller('MainController', ['$scope', '$ionicHistory', '$cordovaCamera', '$ionicPopup', '$window', 'NotificationService', 'PictureService', MainController]);
// function MainController($scope, $ionicHistory, $cordovaCamera, $ionicPopup, $window, NotificationService, PictureService) {

  $scope.navigate = function (url) {
    $window.location = url;
  };

  $scope.back = function () {
    $window.history.back();
  };

  $scope.showBackButton = function () {
    // Workaround for the lib bug of $ionicNavBarDelegate.showBackButton() doesn't work
    return $ionicHistory.currentView() && $ionicHistory.currentView().index > 0;
  }

  $scope.getPicture = function () {
    var picture = PictureService.getMain();
    if (picture) {
      return "data:image/jpeg;base64," + picture;
    }
    return "img/ios-contact.png";
  }

  $scope.hasUnreadNotifications = function () {
    return NotificationService.hasUnreadNotifications();
  }

  $scope.takePhoto = function () {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: true
    };

    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
      PictureService.put(imageData);
    }, function (err) {
      console.error(err);
    });
  }

  $scope.choosePhoto = function () {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: true
    };

    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
      PictureService.put(imageData);
    }, function (err) {
      console.error(err);
    });
  }

  $scope.showTakeOrChoosePhotoPopup = function () {
    $scope.data = {}

    var takeOrChoosePhotoPopup = $ionicPopup.show({
      title: 'Do you want to take or choose a photo?',
      scope: $scope,
      // cssClass: 'basic-popup',

      buttons: [
        {
          text: 'Choose',
          type: 'button-small basic-button button',
          // onTap: $scope.choosePhoto()
          onTap: function (e) {
            $scope.choosePhoto();
          }
        },
        {
          text: 'Take',
          type: 'button-small basic-button button',
          // onTap: $scope.takePhoto()
          onTap: function (e) {
            $scope.takePhoto();
          }
        }
      ]
    });

    // takeOrChoosePhotoPopup.then(function (res) {
      //console.log('Tapped!', res);
    // });
  };

});
  // }
// })();